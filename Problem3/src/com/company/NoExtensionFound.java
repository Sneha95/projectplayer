package com.company;

public class NoExtensionFound extends Exception {

    public NoExtensionFound(String message) {
        super(message);
    }

}
