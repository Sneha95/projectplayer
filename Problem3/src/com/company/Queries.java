package com.company;

import java.sql.*;

public class Queries {

    private  AudioPlayer[] audio;
    private VideoPlayer[] video;
    private DualPlayer[] dualplayer;

    public Queries(){

        audio = new AudioPlayer[50];
        video = new VideoPlayer[50];
        dualplayer= new DualPlayer[50];
    }






    Database d = new Database();
    Connection c = d.db();


    public void queryaudio() throws SQLException {


        String query = "SELECT PlayerName FROM PlayerList WHERE PlayerType ='audio'";
        Statement st = c.createStatement();
        ResultSet rs = null;
        rs = st.executeQuery(query);
        int i = 0;
        while (rs.next()) {

            AudioPlayer aud = new AudioPlayer(rs.getString("PlayerName"));


            audio[i] = aud;

            i++;
        }
        for (int j = 0; j < i; j++)
            audio[j].display();

    }
        public void querydual() throws SQLException {
            String query = "SELECT PlayerName FROM PlayerList WHERE PlayerType ='dual'";
            Statement st = c.createStatement();
            ResultSet rs = null;
            rs = st.executeQuery(query);
            int i=0;
            while (rs.next()) {

                DualPlayer dual = new DualPlayer(rs.getString("PlayerName"));
                dualplayer[i] = dual;
                i++;
            }
            for (int j = 0; j < i; j++)
                dualplayer[j].display();
            c.close();

            }

    public void queryvideo() throws SQLException {
        String query = "SELECT PlayerName FROM PlayerList WHERE PlayerType ='video'";
        Statement st = c.createStatement();
        ResultSet rs = null;
        rs = st.executeQuery(query);
        int i=0;
        while (rs.next()) {

            VideoPlayer dual = new VideoPlayer(rs.getString("PlayerName"));
            video[i] = dual;
            i++;
        }
        for (int j = 0; j < i; j++)
            video[j].display();

    }

    }



